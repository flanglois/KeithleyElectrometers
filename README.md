# KeithleyElectrometers
(Migrated from https://svn.code.sf.net/p/tango-ds/code/DeviceClasses/MeasureInstruments/Keithley/KeithleyElectrometers)

This class allows you to control Keithley electrometers
( with either DDC or SCPI commands ) through GPIB.

Supported types :

Keithley Electrometers : DDC types -> 485, 486, 487, 617, 6512

Keithley Electrometers : SCPI types -> 6485, 6487, 6514, 6517 
