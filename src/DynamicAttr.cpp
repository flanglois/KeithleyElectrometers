#include <iostream>
#include <math.h> //- sqrt
#include "Xstring.h"
#include "DynamicAttr.h"

namespace KeithleyElectrometers_ns
{
const unsigned short BAD_CHANNEL_NUMBER = 0xFFFF;
const double _NaN_ = ::sqrt(-1.);

//- SCALAR CTOR
KeithleyRODynAttr::KeithleyRODynAttr (std::string devName)
  : dp(0)
{
//	std::cout << "KeithleyRODynAttr::KeithleyRODynAttr <-" << std::endl;
  try
  {
    if ( !devName.empty() )
      this->dp = new Tango::DeviceProxy(devName);
  }
  catch (std::bad_alloc)
  {
    Tango::Except::throw_exception(static_cast <const char*> ("MEMORY_ERROR"),
                                   static_cast <const char*> ("device proxy allocation failed (bad_alloc)!"),
                                   static_cast <const char*> ("KeithleyRODynAttr::KeithleyRODynAttr"));
  }
  catch (Tango::DevFailed& e)
  {
    throw (e);
  }
  catch (...)
  {
    Tango::Except::throw_exception(static_cast <const char*> ("MEMORY_ERROR"),
                                   static_cast <const char*> ("device proxy allocation failed (...)!"),
                                   static_cast <const char*> ("KeithleyRODynAttr::KeithleyRODynAttr"));
  }
}

// ============================================================================
// KeithleyRODynAttr::~KeithleyRODynAttr
// ============================================================================
KeithleyRODynAttr::~KeithleyRODynAttr (void)
{
//	std::cout << "KeithleyRODynAttr::~KeithleyRODynAttr <-" << std::endl;
  //- delete device proxy
  if (dp)
  {
    delete dp;
    dp = 0;
  }

//	std::cout << "KeithleyRODynAttr::~KeithleyRODynAttr ->" << std::endl;
}


/******************************************************************************/
/*                                                                            */
/*                ---  KEITHLEY SCALAR ATTRIBUTES   ---                       */
/*                                                                            */
/******************************************************************************/

// ============================================================================
// channelNumberAttr::channelNumberAttr   -   Class implementation
//
//  This attribute is the complete name of the filter
// ============================================================================
channelNumberAttr::channelNumberAttr(const std::string& attr_name, std::string deviceName)
  : KeithleyRODynAttr(deviceName),
    Attr(attr_name.c_str(), Tango::DEV_USHORT, Tango::READ_WRITE),
    _read_part(0),
    _write_part(0)
{
  //	Attribute : isOverloadRange
  Tango::UserDefaultAttrProp	channelNumberAttr_prop;
  channelNumberAttr_prop.set_label("channelNumber");
  channelNumberAttr_prop.set_format("%2d");
  this->set_default_properties(channelNumberAttr_prop);
}

void channelNumberAttr::read(Tango::DeviceImpl* dev,Tango::Attribute& att)
{
  unsigned short chNum = BAD_CHANNEL_NUMBER;
  std::string chNumStr("0");
  std::string description("");
  std::string cmd ("ROUT:CLOS:STAT?");
  Tango::DeviceData argin;
  Tango::DeviceData argout;

  try
  {
    if ( this->dp )
    {
      argin << cmd;
      argout = this->dp->command_inout("WriteRead",  argin);
      //argout = this->dp->command_inout("WriteRead",  cmd, chNumStr);
      argout >> chNumStr;
      //- the response : "(@x)" , where x is the channel number
      chNumStr = chNumStr.substr(2,1);
      chNum = XString<unsigned short>::convertFromString(chNumStr);
    }
  }
  catch (Tango::DevFailed& df)
  {
    description = "Unable to write command : " + cmd + " and read device response.";
    Tango::Except::re_throw_exception (df,
                                       (const char*)"COMMUNICATION_ERROR",
                                       description.c_str(),
                                       (const char*)"channelNumberAttr::read");
  }
  catch (...)
  {
    std::cout << "channelNumberAttr::read failed [...]." << std::endl;
    throw;
  }

  att.set_value(& chNum);
}

void channelNumberAttr::write(Tango::DeviceImpl* dev,Tango::WAttribute& att)
{
  std::stringstream cmd_to_send;
  std::string description("");
  Tango::DevUShort src = 0;
  Tango::DeviceData argin;
  Tango::DeviceData argout;
  Tango::DevString argStr = 0;

  try
  {
    att.get_write_value(src);
    //- channel number must be 1 <= chNum <= 10
    if ( !src || (src > 10) )
    {
      Tango::Except::throw_exception(static_cast <const char*> ("DATA_OUT_OF_RANGE"),
                                     static_cast <const char*> ("channel number must be in the range [1-10]"),
                                     static_cast <const char*> ("channelNumberAttr::write"));
    }

    cmd_to_send << "ROUTe:CLOSe (@" << src << ")" << std::endl;
    std::string command = cmd_to_send.str();
    argStr = new char[command.size() + 1];
    strcpy(argStr, command.c_str());

    argin << argStr;

    if ( this->dp )
      this->dp->command_inout("Write",  argin);
  }
  catch (Tango::DevFailed& df)
  {
    if ( argStr )
    {
      delete [] argStr;
      argStr = 0;
    }
    description = "Unable to write command : " + cmd_to_send.str();
    Tango::Except::re_throw_exception (df,
                                       (const char*)"COMMUNICATION_ERROR",
                                       description.c_str(),
                                       (const char*)"channelNumberAttr::write");
  }
  catch (...)
  {
    if ( argStr )
    {
      delete [] argStr;
      argStr = 0;
    }
    throw;
  }
}

bool channelNumberAttr::is_allowed(Tango::DeviceImpl* dev,Tango::AttReqType ty)
{
  if (dev->get_state() == Tango::FAULT)
  {
    //	End of Generated Code

    //	Re-Start of Generated Code
    return false;
  }
  return true;
}

// ============================================================================
// vSourceAttr::vSourceAttr   -   Class implementation
//
//  This attribute is the complete name of the filter
// ============================================================================
vSourceAttr::vSourceAttr(const std::string& attr_name, std::string deviceName)
  : KeithleyRODynAttr(deviceName),
    Attr(attr_name.c_str(), Tango::DEV_DOUBLE, Tango::READ_WRITE),
    _read_part(0.),
    _write_part(0.)
{
  //	Attribute : isOverloadRange
  Tango::UserDefaultAttrProp	vSourceAttr_prop;
  vSourceAttr_prop.set_label(attr_name.c_str());
  vSourceAttr_prop.set_format("%6.2f");
  this->set_default_properties(vSourceAttr_prop);
}

void vSourceAttr::read(Tango::DeviceImpl* dev,Tango::Attribute& att)
{
  double volts = _NaN_;
  std::string voltsStr("0.");
  std::string description("");
  std::string cmd ("SOUR:VOLT?");
  Tango::DeviceData argin;
  Tango::DeviceData argout;

  try
  {
    if ( this->dp )
    {
      argin << cmd;
      argout = this->dp->command_inout("WriteRead",  argin);
      argout >> voltsStr;
      volts = XString<double>::convertFromString(voltsStr);
    }
  }
  catch (Tango::DevFailed& df)
  {
    description = "Unable to write command : " + cmd + " and read device response.";
    Tango::Except::re_throw_exception (df,
                                       (const char*)"COMMUNICATION_ERROR",
                                       description.c_str(),
                                       (const char*)"vSourceAttr::read");
  }
  catch (...)
  {
    std::cout << "vSourceAttr::read failed [...]." << std::endl;
    throw;
  }

  att.set_value(& volts);
}

void vSourceAttr::write(Tango::DeviceImpl* dev,Tango::WAttribute& att)
{
  std::stringstream cmd_to_send;
  std::string description("");
  Tango::DevDouble src = 0.;
  Tango::DeviceData argin;
  Tango::DevString argStr = 0;

  try
  {
    att.get_write_value(src);

    cmd_to_send << "SOUR:VOLT " << src << std::endl;

    std::string command = cmd_to_send.str();
    argStr = new char[command.size() + 1];
    strcpy(argStr, command.c_str());

    argin << argStr;

    if ( this->dp )
      this->dp->command_inout("Write",  argin);
  }
  catch (Tango::DevFailed& df)
  {
    if ( argStr )
    {
      delete [] argStr;
      argStr = 0;
    }

    description = "Unable to write command : " + cmd_to_send.str();
    Tango::Except::re_throw_exception (df,
                                       (const char*)"COMMUNICATION_ERROR",
                                       description.c_str(),
                                       (const char*)"vSourceAttr::write");
  }
  catch (...)
  {
    if ( argStr )
    {
      delete [] argStr;
      argStr = 0;
    }

    throw;
  }
}

bool vSourceAttr::is_allowed(Tango::DeviceImpl* dev,Tango::AttReqType ty)
{
  if (dev->get_state() == Tango::FAULT)
  {
    //	End of Generated Code

    //	Re-Start of Generated Code
    return false;
  }
  return true;
}

} //- end namespace
