#ifndef _ATTR_READ_ONLY_H
#define _ATTR_READ_ONLY_H

#include <tango.h>
//#include <DeviceProxyHelper.h>

namespace KeithleyElectrometers_ns
{
	//=====================================
	//	Define classes for attributes
	//=====================================
	class KeithleyRODynAttr
	{
	public:
		//- SCALAR Attr
		KeithleyRODynAttr(std::string devName); //, ,const std::string& attr_name,long type, Tango::AttrWriteType rw_mode);
		
		virtual ~KeithleyRODynAttr();
		
	protected :
  	Tango::DeviceProxy* dp;

  private :
    std::string _devName;
	};
	
	
	//=======================================
	//	Define classes for SCALAR attributes
	//=======================================
	//-	channel number attribute
	//=======================================
	class channelNumberAttr: public KeithleyRODynAttr, public Tango::Attr
	{
	public:
    channelNumberAttr( const std::string& attr_name, std::string ds_name);
		
		virtual ~channelNumberAttr() {};
		
		virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att);
		virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att);
		virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty);
		
	protected :
		unsigned short _read_part;
		unsigned short _write_part;
	};
	//=======================================
	//-	vSource attribute
	//=======================================
	class vSourceAttr: public KeithleyRODynAttr, public Tango::Attr
	{
	public:
    vSourceAttr( const std::string& attr_name, std::string ds_name);
		
		virtual ~vSourceAttr() {};
		
		virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att);
		virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att);
		virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty);
		
	protected :
		double _read_part;
		double _write_part;
	};
	
	
}	//	namespace KeithleyElectrometers_ns

#endif // _ATTR_READ_ONLY_H
