/*----- PROTECTED REGION ID(KeithleyElectrometers.h) ENABLED START -----*/
//=============================================================================
//
// file :        KeithleyElectrometers.h
//
// description : Include file for the KeithleyElectrometers class
//
// project :     Keithley Electrometer
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#ifndef KeithleyElectrometers_H
#define KeithleyElectrometers_H

#include <tango.h>
#include "AbstractElectrometerClass.h"
#include "ElectrometerException.h"
#include "DynamicAttr.h"




/*----- PROTECTED REGION END -----*/	//	KeithleyElectrometers.h

/**
 *  KeithleyElectrometers class description:
 *    This class allows you to control all Keithley electrometers
 *    ( with either DDC or SCPI commands ) through a GPIB.
 *    <br> Supported types :
 *    <br> Keithley Electrometers : DDC types -> 485, 486, 487, 617, 6512
 *    <br> Keithley Electrometers : SCPI types -> 6485, 6487, 6514, 6517
 */

namespace KeithleyElectrometers_ns
{
/*----- PROTECTED REGION ID(KeithleyElectrometers::Additional Class Declarations) ENABLED START -----*/

//	Additional Class Declarations

/*----- PROTECTED REGION END -----*/	//	KeithleyElectrometers::Additional Class Declarations

class KeithleyElectrometers : public TANGO_BASE_CLASS
{

/*----- PROTECTED REGION ID(KeithleyElectrometers::Data Members) ENABLED START -----*/

//	Add your own data members
public:



/*----- PROTECTED REGION END -----*/	//	KeithleyElectrometers::Data Members

//	Device property data members
public:
	//	CommunicationLinkName:	The name of the device which manage the communication.
	string	communicationLinkName;
	//	ElectrometerType:	The elctrometer type number.
	//  -> for 485 Keithley model : 485
	Tango::DevLong	electrometerType;
	//	UseMultiplexedChannels:	Set to true if a channel multiplexer is connected (such as a 6521 module option for 6517 Keithley device).
	Tango::DevBoolean	useMultiplexedChannels;
	//	CreateDynamicVSourceAttribute:	Only for those models which support VSource output feature (as 6517).
	Tango::DevBoolean	createDynamicVSourceAttribute;

//	Attribute data members
public:
	Tango::DevDouble	*attr_value_read;
	Tango::DevDouble	*attr_range_read;
	Tango::DevString	*attr_mode_read;
	Tango::DevBoolean	*attr_isAutoRangeOn_read;
	Tango::DevString	*attr_rangesList_read;

//	Constructors and destructors
public:
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	KeithleyElectrometers(Tango::DeviceClass *cl,string &s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	KeithleyElectrometers(Tango::DeviceClass *cl,const char *s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device name
	 *	@param d	Device description.
	 */
	KeithleyElectrometers(Tango::DeviceClass *cl,const char *s,const char *d);
	/**
	 * The device object destructor.
	 */
	~KeithleyElectrometers() {delete_device();};


//	Miscellaneous methods
public:
	/*
	 *	will be called at device destruction or at init command.
	 */
	void delete_device();
	/*
	 *	Initialize the device
	 */
	virtual void init_device();
	/*
	 *	Read the device properties from database
	 */
	void get_device_property();
	/*
	 *	Always executed method before execution command method.
	 */
	virtual void always_executed_hook();


//	Attribute methods
public:
	//--------------------------------------------------------
	/*
	 *	Method      : KeithleyElectrometers::read_attr_hardware()
	 *	Description : Hardware acquisition for attributes.
	 */
	//--------------------------------------------------------
	virtual void read_attr_hardware(vector<long> &attr_list);
	//--------------------------------------------------------
	/*
	 *	Method      : KeithleyElectrometers::write_attr_hardware()
	 *	Description : Hardware writing for attributes.
	 */
	//--------------------------------------------------------
	virtual void write_attr_hardware(vector<long> &attr_list);

/**
 *	Attribute value related methods
 *	Description: Switch the selected mode it represents the value(s) returned in\n- amp if the AmpereMeter function is selected\n- volt in Volt Meter mode\n- ohm in Ohm Meter mode\n- coulomb in Coulomb Meter mode
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
	virtual void read_value(Tango::Attribute &attr);
	virtual bool is_value_allowed(Tango::AttReqType type);
/**
 *	Attribute range related methods
 *	Description: Range of the selected mode
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
	virtual void read_range(Tango::Attribute &attr);
	virtual void write_range(Tango::WAttribute &attr);
	virtual bool is_range_allowed(Tango::AttReqType type);
/**
 *	Attribute mode related methods
 *	Description: The selected mode
 *
 *	Data type:	Tango::DevString
 *	Attr type:	Scalar
 */
	virtual void read_mode(Tango::Attribute &attr);
	virtual bool is_mode_allowed(Tango::AttReqType type);
/**
 *	Attribute isAutoRangeOn related methods
 *	Description: True if auto range ON.
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_isAutoRangeOn(Tango::Attribute &attr);
	virtual bool is_isAutoRangeOn_allowed(Tango::AttReqType type);
/**
 *	Attribute rangesList related methods
 *	Description: Ranges list of the selected mode
 *
 *	Data type:	Tango::DevString
 *	Attr type:	Spectrum max = 64
 */
	virtual void read_rangesList(Tango::Attribute &attr);
	virtual bool is_rangesList_allowed(Tango::AttReqType type);


	//--------------------------------------------------------
	/**
	 *	Method      : KeithleyElectrometers::add_dynamic_attributes()
	 *	Description : Add dynamic attributes if any.
	 */
	//--------------------------------------------------------
	void add_dynamic_attributes();




//	Command related methods
public:
	/**
	 *	Command State related method
	 *	Description: This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
	 *
	 *	@returns State Code
	 */
	virtual Tango::DevState dev_state();
	/**
	 *	Command Status related method
	 *	Description: This command gets the device status (stored in its <i>device_status</i> data member) and returns it to the caller.
	 *
	 *	@returns Status description
	 */
	virtual Tango::ConstDevString dev_status();
	/**
	 *	Command RangeUP related method
	 *	Description: Up the range of the electrometer.
	 *               Throw :
	 *               - electrometer::ElectrometerException if the range limit is reached
	 *               - Tango::DevFailed if the command cannot be performed
	 *
	 */
	virtual void range_up();
	virtual bool is_RangeUP_allowed(const CORBA::Any &any);
	/**
	 *	Command RangeDOWN related method
	 *	Description: Down the range of the electrometer.
	 *               Throw :
	 *               - electrometer::ElectrometerException if the range is negative
	 *               - Tango::DevFailed if the command cannot be performed
	 *
	 */
	virtual void range_down();
	virtual bool is_RangeDOWN_allowed(const CORBA::Any &any);
	/**
	 *	Command AutoRangeON related method
	 *	Description: Command to switch the electrometer auto range ON
	 *
	 */
	virtual void auto_range_on();
	virtual bool is_AutoRangeON_allowed(const CORBA::Any &any);
	/**
	 *	Command AutoRangeOFF related method
	 *	Description: Command to switch the electrometer auto range OFF
	 *
	 */
	virtual void auto_range_off();
	virtual bool is_AutoRangeOFF_allowed(const CORBA::Any &any);
	/**
	 *	Command ZeroCheckON related method
	 *	Description: Command to switch the electrometer zero check ON
	 *
	 */
	virtual void zero_check_on();
	virtual bool is_ZeroCheckON_allowed(const CORBA::Any &any);
	/**
	 *	Command ZeroCheckOFF related method
	 *	Description: Command to switch the electrometer zero check OFF
	 *
	 */
	virtual void zero_check_off();
	virtual bool is_ZeroCheckOFF_allowed(const CORBA::Any &any);
	/**
	 *	Command ZeroCorrectON related method
	 *	Description: Command to switch the electrometer zero correct ON
	 *
	 */
	virtual void zero_correct_on();
	virtual bool is_ZeroCorrectON_allowed(const CORBA::Any &any);
	/**
	 *	Command ZeroCorrectOFF related method
	 *	Description: Command to switch the electrometer zero correct OFF
	 *
	 */
	virtual void zero_correct_off();
	virtual bool is_ZeroCorrectOFF_allowed(const CORBA::Any &any);
	/**
	 *	Command AutoZeroON related method
	 *	Description: To help maintain stability and accuracy over time and changes in temperature, the
	 *               Model 6487 periodically measures internal voltages corresponding to offsets (zero) and
	 *               amplifier gains. These measurements are used in the algorithm to calculate the reading of
	 *               the input signal.
	 *
	 */
	virtual void auto_zero_on();
	virtual bool is_AutoZeroON_allowed(const CORBA::Any &any);
	/**
	 *	Command AutoZeroOFF related method
	 *	Description: When autozero is disabled, the offset and gain measurements are not performed. This
	 *               increases measurement speed up to three times. However, the zero and gain reference
	 *               points can eventually drift resulting in inaccurate readings of the input signal. It is recommended
	 *               that autozero only be disabled for short periods of time.
	 *
	 */
	virtual void auto_zero_off();
	virtual bool is_AutoZeroOFF_allowed(const CORBA::Any &any);
	/**
	 *	Command SetAmperMeterMode related method
	 *	Description: If supported, this command switch the electrometer in AmperMeter mode
	 *
	 */
	virtual void set_amper_meter_mode();
	virtual bool is_SetAmperMeterMode_allowed(const CORBA::Any &any);
	/**
	 *	Command SetVoltMeterMode related method
	 *	Description: If supported, this command switch the electrometer in VoltMeter mode.
	 *
	 */
	virtual void set_volt_meter_mode();
	virtual bool is_SetVoltMeterMode_allowed(const CORBA::Any &any);
	/**
	 *	Command SetOhmMeterMode related method
	 *	Description: If supported, this command switch the electrometer in OhmMeter mode.
	 *
	 */
	virtual void set_ohm_meter_mode();
	virtual bool is_SetOhmMeterMode_allowed(const CORBA::Any &any);
	/**
	 *	Command SetCoulombMeterMode related method
	 *	Description: If supported, this command switch the electrometer in Coulomb Meter mode.
	 *
	 */
	virtual void set_coulomb_meter_mode();
	virtual bool is_SetCoulombMeterMode_allowed(const CORBA::Any &any);
	/**
	 *	Command ClearRegisters related method
	 *	Description: Clear all registers.
	 *
	 */
	virtual void clear_registers();
	virtual bool is_ClearRegisters_allowed(const CORBA::Any &any);
	/**
	 *	Command AverageStateON related method
	 *	Description: Enable digital filter
	 *
	 */
	virtual void average_state_on();
	virtual bool is_AverageStateON_allowed(const CORBA::Any &any);
	/**
	 *	Command AverageStateOFF related method
	 *	Description: Disable digital filter
	 *
	 */
	virtual void average_state_off();
	virtual bool is_AverageStateOFF_allowed(const CORBA::Any &any);
	/**
	 *	Command ClearBuffer related method
	 *	Description: This command clears all data previously stored.
	 *
	 */
	virtual void clear_buffer();
	virtual bool is_ClearBuffer_allowed(const CORBA::Any &any);
	/**
	 *	Command Start related method
	 *	Description: Trigger one or more reading(s)
	 *
	 */
	virtual void start();
	virtual bool is_Start_allowed(const CORBA::Any &any);
	/**
	 *	Command Reset related method
	 *	Description: Reset to factory settings
	 *
	 */
	virtual void reset();
	virtual bool is_Reset_allowed(const CORBA::Any &any);
	/**
	 *	Command Abort related method
	 *	Description: SCPI command only
	 *               Cancels all operations started by Start command
	 *
	 */
	virtual void abort();
	virtual bool is_Abort_allowed(const CORBA::Any &any);
	/**
	 *	Command EnableSRQBufferFull related method
	 *	Description: Sets Buffer Full bit in the MEASUREMENT REGISTER EVENT.
	 *               To know if the integration is finished, check :
	 *               - if the attribute isDataAvailable is true
	 *               or
	 *               - if the device state is STANDBY
	 *
	 */
	virtual void enable_srqbuffer_full();
	virtual bool is_EnableSRQBufferFull_allowed(const CORBA::Any &any);
	/**
	 *	Command DisableSRQBufferFull related method
	 *	Description: Resets all events in the device registers.
	 *
	 */
	virtual void disable_srqbuffer_full();
	virtual bool is_DisableSRQBufferFull_allowed(const CORBA::Any &any);
	/**
	 *	Command GetKeithleyConfig related method
	 *	Description: Returns the Keithley DDC model configuration such as TriggerMode, mode, ....
	 *
	 *	@returns DDC Keithley configuration
	 */
	virtual Tango::DevString get_keithley_config();
	virtual bool is_GetKeithleyConfig_allowed(const CORBA::Any &any);
	/**
	 *	Command GetRange related method
	 *	Description: Returns the electrometer range value.
	 *
	 *	@returns The electrometer range value
	 */
	virtual Tango::DevString get_range();
	virtual bool is_GetRange_allowed(const CORBA::Any &any);
	/**
	 *	Command SetRange related method
	 *	Description: Sets the new electrometer range value.
	 *
	 *	@param argin Index of the new range value to set (cf rangesList atrribute to get ranges indexes).
	 */
	virtual void set_range(Tango::DevUShort argin);
	virtual bool is_SetRange_allowed(const CORBA::Any &any);
	/**
	 *	Command GetMode related method
	 *	Description: Returns the electrometer mode.
	 *
	 *	@returns The electrometer mode
	 */
	virtual Tango::DevString get_mode();
	virtual bool is_GetMode_allowed(const CORBA::Any &any);
	/**
	 *	Command GetIntegrationTime related method
	 *	Description: Returns the electrometer integration time (in seconds).
	 *
	 *	@returns the integration time value
	 */
	virtual Tango::DevDouble get_integration_time();
	virtual bool is_GetIntegrationTime_allowed(const CORBA::Any &any);
	/**
	 *	Command SetIntegrationTime related method
	 *	Description: Sets the new electrometer integration time value (in seconds).
	 *
	 *	@param argin The integration time value (in seconds)
	 */
	virtual void set_integration_time(Tango::DevDouble argin);
	virtual bool is_SetIntegrationTime_allowed(const CORBA::Any &any);
	/**
	 *	Command VSourceOutputON related method
	 *	Description: Enables the V-SOURCE output
	 *
	 */
	virtual void vsource_output_on();
	virtual bool is_VSourceOutputON_allowed(const CORBA::Any &any);
	/**
	 *	Command VSourceOutputOFF related method
	 *	Description: Disables the V-SOURCE output
	 *
	 */
	virtual void vsource_output_off();
	virtual bool is_VSourceOutputOFF_allowed(const CORBA::Any &any);
	/**
	 *	Command SaveConfiguration related method
	 *	Description: Save the Keithley configuration.
	 *
	 *	@param argin Configuration index number
	 */
	virtual void save_configuration(Tango::DevUShort argin);
	virtual bool is_SaveConfiguration_allowed(const CORBA::Any &any);
	/**
	 *	Command RestoreConfiguration related method
	 *	Description: Restores the saved Keithley configuration, stored at the specified index.
	 *
	 *	@param argin Configuration index to restore
	 */
	virtual void restore_configuration(Tango::DevUShort argin);
	virtual bool is_RestoreConfiguration_allowed(const CORBA::Any &any);


	//--------------------------------------------------------
	/**
	 *	Method      : KeithleyElectrometers::add_dynamic_commands()
	 *	Description : Add dynamic commands if any.
	 */
	//--------------------------------------------------------
	void add_dynamic_commands();

/*----- PROTECTED REGION ID(KeithleyElectrometers::Additional Method prototypes) ENABLED START -----*/

//	Additional Method prototypes
protected :	
	AbstractElectrometerClass*	_electrometer;
	
	std::string _statusStr;
	
	bool _kSCPItype;						//- is a SCPI Keithley
	bool _init_failed;
	bool _integrationStarted;
	bool _is_srqbuffer_full_enabled;
	
  //- dynamic Attributes creation
  void create_dynamic_attributes (void);
	void add_dynamic_attribute(Tango::Attr* attr);
	void remove_dynamic_attibutes();
	std::vector<Tango::Attr*> _added_attributes;        //- add/remove_attribute dynamics attributes
	std::vector<std::string> _added_attribute_name;		//- remove dynamics attributes from the database
	
	//- Method to convert all electrometer exceptions on Tango exception
	Tango::DevFailed electrometer_to_tango_exception(const electrometer::ElectrometerException& de);

private :
	void create_keithleyModel_object(void);

/*----- PROTECTED REGION END -----*/	//	KeithleyElectrometers::Additional Method prototypes
};

/*----- PROTECTED REGION ID(KeithleyElectrometers::Additional Classes Definitions) ENABLED START -----*/

//	Additional Classes Definitions

/*----- PROTECTED REGION END -----*/	//	KeithleyElectrometers::Additional Classes Definitions

}	//	End of namespace

#endif   //	KeithleyElectrometers_H
